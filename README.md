Nguời viết: Hiển

# Học Python cho devops
## Tổng quan
* Khác với công việc một programer, devops đặc thù không cần code nhiều nên cũng ko cần hiểu sâu về Python này.
* Cơ bản chúng ta chỉ cần học những gì chúng ta cần dùng với nó.
* Python dành cho devops chỉ cần đáp ứng 3 mục tiếu sau là đủ:
  * Biến, kiểu dự liệu, hàm, mảng ( khai báo, và sử dụng biến, hàm, array, dictionary, turtle, list)
  * Xử lý chuỗi, file, folder, chuỗi trong file
  * Thực thi shell scrip(thực thi lệnh, rút trích dữ liệu hệ thống) 

## Tìm hiểu
### Đây là ví dụ mẫu :
```
1. #!/usr/bin/env python  # goi trình thông dịch python
2.
3. from git import Repo     # gọi module support git 
4. import subprocess    # gọi module thực thi shell
5. # Hàm replace tất cả chuỗi trong file 
6. def replace_string_in_file(fileName, strSrc, strDes ):
7.    with open(fileName, "rt") as fin:     # mở file với mode đọc
8.        with open(fileName + ".cp", "wt") as fout: # mở file ghi
9.            for line in fin:      # vòng lặp đọc file
10.               fout.write(line.replace(strSrc, strDes)) # ghi file
11. def find_string_in_file(fileName, str):
12. 	with open(fileName, "rt") as fin:
13.        for line in fin:
14.            pos = line.find(str) # Tìm chuỗi con trong chuỗi
15.            if pos > 0:      # điều kiện
16.                return pos
17.        return -1
18.
19. path = "python"     # khai báo chuỗi
20. repo = Repo(path)       # gán giá trị hàm vào biến
21. origin = repo.remote('origin')
22.
23. print("pull code ve!")  # In chuỗi ra màn hình
24. origin.pull()       # pull code từ git về
25.
26. strFind = raw_input("nhap chuoi can tim: ") # Nhập từ bàn phím
27. print("vi tri chuoi: %d")%(find_string_in_file("/path/to/repo/httpd.conf",strFind ))
28. strSrc = raw_input("nhap chuoi can thay: ")
29. strDes = raw_input("nhap chuoi thay: ")
30. # Gọi hàm xử lý
31. replace_string_in_file("/path/to/repo/httpd.conf", strSrc, strDes)
32.
33. print("push code len!")
# khai báo mảng
34. file_list = ['/path/to/repo/httpd.conf', '/path/to/repo/my.cnf' ]
35. sms = raw_input("Please type message: ")
36. repo.index.add(file_list)
37. repo.index.commit(sms)
38.
39. origin.push()       # đẩy code lên git
40.
```
Ví dụ trên thực hiện một số thao tác như, pull code từ git, chỉnh sử file http.conf và push code lên lại
### Biến và kiểu dữ liệu
* Biến được khai báo như sau:
```
[tên biến] = [giá trị]
# ví dụ
a = 111
b = 11.1
c = "một trắm mười một"
d = [111, 11, 1]
# kiểm tra kiểu dữ liệu của biến
type(a) => int
type(b) => float
type(d) => array
type(c) => string
...
```
* Xem các dòng khai báo và gán giá trị cho biến trong ví dụ trên: 19, 20, 21, 49, 50 
* khai báo hàm
```
# cú Pháp:
def [tên hàm]([danh sách biến]):
    xử lý hàm
Ví dụ hàm sửa chuỗi trong file 
def replace_string_in_file(fileName, strSrc, strDes ):
    with open(fileName, "rt") as fin:
        with open(fileName + ".cp", "wt") as fout:
            for line in fin:
                fout.write(line.replace(strSrc, strDes))
```
* Xem một số ví dụ khai báo hàm ở ví dụ trên 6, 11
* Xử lý mảng
  * [Thư viện tra cứu](https://docs.python.org/2/library/array.html?highlight=array#module-array)
  
```
arr = [1, 2, 3, 4, 5]
# truy xuất
arr[0] => 1
arr[-1] => 5
arr[1:3] => [2,3,4]
arr[1:4:2] => [2,4]
# vòng lặp 
for i in a:
   print i 
```
### Xử lý chuỗi, file
* xử lý chuỗi
  * Thư viện chính tra cứu: [ link ở đây](https://docs.python.org/2/library/string.html)
  * [Một số thao tác và hàm cơ bản về chuỗi](http://vietjack.com/python/string_trong_python.jsp)
* Xử lý file 
  * [Xử lý file cơ bản](http://vietjack.com/python/file_io_trong_python.jsp)
  * [thư viện thao tác file](http://vietjack.com/python/string_trong_python.jsp)
  * [Thư viện thao tác thư mục](http://vietjack.com/python/phuong_thuc_thao_tac_file_io_trong_python.jsp)

### Thực thi shell script
Sử dụng module [subprocess](https://docs.python.org/2/library/subprocess.html) để thực hiện các lệnh shell
```
import subprocess
# Ví dụ:
# Liệt kê thư mục:
subprocess.call("ls -la", shell=True)
# Thực thi file scrip với tham số
subprocess.call("./echo.sh param1 param2", shell=True)

```
